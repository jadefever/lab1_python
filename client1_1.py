import socket
import sys

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)        
socket.connect(("localhost", 1488))                                

while True:
    data = input(' Say: ')                          
    if not data or data=="/close_connection":                          
        socket.close()                                             
        print("\n Connection closed")
        sys.exit(1)
    data = str.encode(data)                                            
    socket.send(data)                                              
    data = bytes.decode(data)
    data = socket.recv(1024)                                       
    print(data)                                                       
    print()
