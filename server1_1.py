import socket
import time

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)       
socket.bind(("localhost", 1488))                                 
socket.listen(1)                                                  

print(' Connecting...')
connection, address = socket.accept()                             
print(' Connected')
print(' Connected with: ', address)                               
while True:
    data = connection.recv(1024)                                       
    newData = bytes.decode(data)                                      
    if not data or newData == "/close_connection":                    
        connection.close()                                            
        print(" Connection closed")
        break
    else:
        print(" Client say: " + newData)                    
        newData = " Server say: "+newData     
        time.sleep(5)                                                  
        print(newData)                                                 
        data = str.encode(newData)                                   
        connection.send(data)                                         
        print("")

socket.close()  